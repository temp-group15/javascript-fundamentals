const userNameElement = document.getElementById("userName");
const balanceElement = document.getElementById("balance");
const getLoanElement = document.getElementById("getLoan");
const payLoanElement = document.getElementById("payLoan");
const payElement = document.getElementById("pay");
const workElement = document.getElementById("work");
const bankElement = document.getElementById("bank");
const computerSelectElement = document.getElementById("computerSelect");
const computerInfoElement = document.getElementById("computerInfo");
const computerImageElement = document.getElementById("computerImage");
const modelNameElement = document.getElementById("modelName");
const modelDescriptionElement = document.getElementById("modelDescription");
const priceElement = document.getElementById("price");
const buyNowElement = document.getElementById("buyNowBtn");

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputers(computers));

let balance = 0;
let sum = 0;
let userLoan = 0;
let hasLoan = false;
let computers = [];
let selectedComputer = 0;

// Makes the getLoan button show a prompt to enter the desired loan amount and output it in NOK
const giveLoan = () => {
    const loanAmount = parseInt(prompt("Please enter a loan amount"));
    if (hasLoan == false && loanAmount <= (balance * 2)) {
        balance += loanAmount;
        userLoan += loanAmount;
        hasLoan = true;
        payLoanElement.style.visibility = "visible";
    }
    else if (hasLoan) {
        alert("You can only have one loan at a time!");
    }
    else if (loanAmount > (balance * 2)) {
        alert("You cannot loan more than double your current balance!");
    }
    else {
        alert("Something went wrong!")
    }
    balanceElement.innerHTML = balance.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });
}

// Makes the pay element increase by 100 each time the work button is pressed and output it in NOK
const increasePay = () => {
    sum = sum + 100;
    payElement.innerHTML = sum.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });
}

// Transfers the current money in Pay to Balance
const payToBank = () => {
    const addToBalance = balance += sum;
    balanceElement.innerHTML = addToBalance;
    sum = 0;
    payElement.innerHTML = sum.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });
    balanceElement.innerHTML = balance.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });
}

// Applies the addcomputerToList function to each entry in the JSON document
const addComputers = (computers) => {
    computers.forEach(x => addComputerToList(x));
}

// Appends option entries to the computerSelect dropdown button to show all the available models
const addComputerToList = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computerSelectElement.appendChild(computerElement);
}


// Appends dt entries to computerInfo with the computer specs to reflect the selected computer
const addComputerSpecs = (computer) => {
    computerInfoElement.textContent = '';
    selectedComputer = computer.target.selectedIndex;
    computers[selectedComputer].specs.forEach(spec => {
        const specsElement = document.createElement("li");
        specsElement.innerText = spec;
        computerInfoElement.appendChild(specsElement);
    })
}

// Update the price, image, model name and description to reflect the selected computer
const changeComputerParameters = a => {
    let selectedComputer = computers[a.target.selectedIndex];
    priceElement.innerHTML = selectedComputer.price.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });
    computerImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
    modelNameElement.innerHTML = selectedComputer.title;
    modelDescriptionElement.innerHTML = selectedComputer.description;
}

// Displays a message upon clicking the Buy now button and subtracts the price from the balance
const buyComputer = () => {
    let selectedPC = computers[selectedComputer];
    if (balance >= selectedPC.price) {
        balance -= selectedPC.price;
        balanceElement.innerHTML = balance;
        alert("Congratulations! You are now the owner of a Kool Komputer™!");
        balanceElement.innerHTML = balance.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });
    }
    else if (balance < selectedComputer.price) {
        alert("You do not have enough money for that computer!");
    }
}

getLoanElement.addEventListener("click", giveLoan);
workElement.addEventListener("click", increasePay);
bankElement.addEventListener("click", payToBank);
computerSelectElement.addEventListener("change", addComputerSpecs);
computerSelectElement.addEventListener("change", changeComputerParameters);
buyNowElement.addEventListener("click", buyComputer);